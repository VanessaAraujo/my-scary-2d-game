﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grid : MonoBehaviour {



	public LayerMask unwalkableMask; // layer of obstacles
	public Vector2 gridWorldSize; // size of grid 
	public float nodeRadius; // size of each node
	Node[,] grid; // array with all nodes, their position and walkable status

	float nodeDiameter; // diameter of node
	int gridSizeX, gridSizeY; // each axis of grid size

	void Start() { // on statrt
		nodeDiameter = nodeRadius*2; //we dont' actually need this
		gridSizeX = Mathf.RoundToInt(gridWorldSize.x/nodeDiameter); // number of square nodes in one row
		gridSizeY = Mathf.RoundToInt(gridWorldSize.y/nodeDiameter); // number of square nodes in one colum
		CreateGrid(); // call function
	}

	void CreateGrid() {
		grid = new Node[gridSizeX,gridSizeY]; // initialize the array of nodes with its size
		Vector3 worldBottomLeft = transform.position - Vector3.right * gridWorldSize.x/2 - Vector3.up * gridWorldSize.y/2; // finds bottom left position

		for (int x = 0; x < gridSizeX; x ++) {
			for (int y = 0; y < gridSizeY; y ++) { // each node position from bottom left
				Vector3 worldPoint = worldBottomLeft + Vector3.right * (x * nodeDiameter + nodeRadius) + Vector3.up * (y * nodeDiameter + nodeRadius);
				//bool walkable = !(Physics.CheckSphere(worldPoint,nodeRadius,unwalkableMask)); // check if there is a collider
				bool walkable = !(Physics2D.OverlapCircle(worldPoint,2*nodeRadius,unwalkableMask)); // check if there is a collider
				grid[x,y] = new Node(walkable,worldPoint,x,y);// populate array of nodes

			}
		}
	}

	public List<Node> GetNeighbours(Node node) {
		// worldPos -= Base.GetComponent<MeshRenderer>().bounds.center;  The percentage must be related to the grid position, otherwise it is wrong. Add this if grid not centered at 0,0,0
		List<Node> neighbours = new List<Node>();

		for (int x = -1; x <= 1; x++) {
			for (int y = -1; y <= 1; y++) {
				if (x == 0 && y == 0)
					continue;

				int checkX = node.gridX + x;
				int checkY = node.gridY + y;

				if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY) {
					neighbours.Add(grid[checkX,checkY]);
				}
			}
		}

		return neighbours;
	}

	public Node NodeFromWorldPoint(Vector3 worldPosition) {
		float percentX = (worldPosition.x + gridWorldSize.x/2) / gridWorldSize.x;
		float percentY = (worldPosition.y + gridWorldSize.y/2) / gridWorldSize.y;
		percentX = Mathf.Clamp01(percentX);
		percentY = Mathf.Clamp01(percentY);

		int x = Mathf.RoundToInt((gridSizeX-1) * percentX);
		int y = Mathf.RoundToInt((gridSizeY-1) * percentY);

		return grid[x,y];

	}

	public List<Node> path;
	void OnDrawGizmos() {
		Gizmos.DrawWireCube(transform.position,new Vector3(gridWorldSize.x,gridWorldSize.y,1));// draw the big gizmoz

		if (grid != null) {//ig grid already created
			//Node playerNode = NodeFromWorldPoint (player.position);
			foreach (Node n in grid) {
				Gizmos.color = (n.walkable)?Color.white:Color.red;
				////if(playerNode == n)
				//{
				//	Gizmos.color = Color.cyan;
				//}
				if (path != null)
				if (path.Contains (n)) {
					
					Gizmos.color = Color.black;
				}
				Gizmos.DrawCube(n.worldPosition, Vector3.one * (nodeDiameter-.1f));
			}
		}
	}

}

public class Node { //each node of the grid

	public bool walkable; //Node collider inside node
	public Vector3 worldPosition; // world position of node
	public int gridX;
	public int gridY;

	public int gCost;
	public int hCost;
	public Node parent;// from where it comes on the path

	public Node(bool _walkable, Vector3 _worldPos, int _gridX, int _gridY) {
		walkable = _walkable;
		worldPosition = _worldPos;
		gridX = _gridX;
		gridY = _gridY;
	}

	public int fCost {
		get {
			return gCost + hCost;
		}
	}
}