﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerMovementController : MonoBehaviour {



	private bool facingRight = true;
	private bool isMoving = false;
	//private Vector3 pz;

	public Transform player;// player position
	public Animator anim;
	private Vector3 seeker;
	private Vector3 target;// initial and final position

	private bool walk;
	private int step = 0;
	public float reachDistance = 0.5f;

	List<Node> path = new List<Node>(); // create empty list

	// Update is called once per frame
	PathFinder pathFinder; // reference to Grid class
	Grid grid;

	void Awake() {
		pathFinder = GetComponent<PathFinder>(); // initialize Grid
		grid = GetComponent<Grid>();
		seeker = player.position;//


	}





		void Update() { // call FindPath method, with initial and final position. can not change the seeker position, or it changes the behaviour
		
			if (Input.GetMouseButtonDown (0)) {
			seeker = player.position;//

				path.Clear();
				step = 0;
				target = Camera.main.ScreenToWorldPoint (Input.mousePosition);
			    target.z = 0;//
			    target.y+= 4;

				
				path = pathFinder.FindPath (seeker, target);
			    walk = true;



			}

		anim.SetBool ("walk", isMoving);

			if (walk & (step<path.Count)) {
			
				float distance = Vector3.Distance (player.position, path [step].worldPosition);
				player.position = Vector3.MoveTowards(player.position, path[step].worldPosition, Time.deltaTime * 5f);
			    isMoving = true;

			if( (path[step].worldPosition.x - player.position.x) < 0 && facingRight )
			{
				Flip();
			}
			else if( (path[step].worldPosition.x - player.position.x) > 0 && !facingRight )
			{
				Flip();
			}
				if(distance <= reachDistance)
					step++;
			
			//if(Vector3.Distance(target, player.position)<= reachDistance) 
			if(step>=path.Count||path.Count<=1||path==null||step==0)
				isMoving = false;



			}


		}



	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = player.localScale;
		theScale.x *= -1;
		player.localScale = theScale;

	}
}