﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFinder : MonoBehaviour {
	



	List<Node> path = new List<Node>();
	Grid grid; // reference to Grid class

	void Awake() {
		grid = GetComponent<Grid>(); // initialize Grid
	}

	/*void Update() { // call FindPath method, with initial and final position. can not change the seeker position, or it changes the behaviour
		if (Input.GetMouseButtonDown (0)) {
			target = Camera.main.ScreenToWorldPoint (Input.mousePosition);

			Debug.Log (target);
			FindPath (seeker.position, target);
		}

		if (walk & (step<path.Count)) {
			
			float distance = Vector3.Distance (player.position, path [step].worldPosition);
			player.position = Vector3.MoveTowards(player.position, path[step].worldPosition, Time.deltaTime * 5f);
			if(distance <= reachDistance)
			step++;

		}
	}*/


	public List<Node> FindPath(Vector3 startPos, Vector3 targetPos) {
		
		Node startNode = grid.NodeFromWorldPoint(startPos); // find which node of the grid, corresponds to start position


		Node targetNode = grid.NodeFromWorldPoint(targetPos); // find which node of the grid is the target position

		List<Node> openSet = new List<Node>(); // create new list of nodes, the openset
		List<Node> closedSet = new List<Node>(); // create new list of nodes with closeset
		openSet.Add(startNode); // adds initial node to openset

		while (openSet.Count > 0) { // while there is some node in open set
			Node currentNode = openSet[0]; // take the first of nodes
			for (int i = 1; i < openSet.Count; i ++) {
				if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost) {
					currentNode = openSet[i];
				}
			}

			openSet.Remove(currentNode);
			closedSet.Add(currentNode);

			if (currentNode == targetNode) {
				RetracePath(startNode,targetNode);
				return (path);
			}

			foreach (Node neighbour in grid.GetNeighbours(currentNode)) {
				if (!neighbour.walkable || closedSet.Contains(neighbour)) {
					continue;
				}

				int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
				if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour)) {
					neighbour.gCost = newMovementCostToNeighbour;
					neighbour.hCost = GetDistance(neighbour, targetNode);
					neighbour.parent = currentNode;

					if (!openSet.Contains(neighbour))
						openSet.Add(neighbour);
				}
			}
		}
		return (path);
	}

	void RetracePath(Node startNode, Node endNode) { // create list with path from start to end node
		
		Node currentNode = endNode; // current node equals end node

		while (currentNode != startNode) {
			path.Add(currentNode);

			currentNode = currentNode.parent;
		}
		path.Reverse();

		grid.path = path;
		//walk = true;


	}

	int GetDistance(Node nodeA, Node nodeB) {
		int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
		int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

		if (dstX > dstY)
			return 14*dstY + 10* (dstX-dstY);
		return 14*dstX + 10 * (dstY-dstX);
	}
}
