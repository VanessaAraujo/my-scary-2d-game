﻿using UnityEngine;
using System;
using System.Collections;


public class Flickering : MonoBehaviour
{
	

	float interval =0.3f;

	void Start()

	{
		InvokeRepeating("FlashLabel", 0, interval);
	}


	void FlashLabel()

	{
		
		if(gameObject.activeSelf)
			gameObject.SetActive(false);
		else
			gameObject.SetActive(true);
	}
}
